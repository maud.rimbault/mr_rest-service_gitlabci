package com.example.restservice;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.*;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    private final ArrayList<Greeting> list = new ArrayList<Greeting>();

    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    @GetMapping("/greetingList")
    ArrayList<Greeting> greetingList(@RequestParam(value = "name", defaultValue = "World") String name) {
        return list;
    }

    @PostMapping("/greeting")
    public ArrayList<Greeting> greetingPost(@RequestBody Greeting newGreeting) {
        list.add(newGreeting);
        return list;
    }
}